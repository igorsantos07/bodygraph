<?php
$this->breadcrumbs['Conversion Indexes'] = array('admin');
$this->breadcrumbs[] = $model->id;
?>
<?php $this->widget("TbBreadcrumbs", array("links"=>$this->breadcrumbs)) ?>
<h1>
    Conversion Index <small>View #<?php echo $model->id ?></small></h1>



<?php $this->renderPartial("_toolbar", array("model"=>$model)); ?>

<h2>
    Data
</h2>

<p>
    <?php
    $this->widget('TbDetailView', array(
    'data'=>$model,
    'attributes'=>array(
            'id',
        'index',
        array(
            'name'=>'unit_from',
            'value'=>($model->unitFrom !== null)?'<span class=label>CBelongsToRelation</span><br/>'.CHtml::link($model->unitFrom->name, array('unit/view','id'=>$model->unitFrom->id), array('class'=>'btn')):'n/a',
            'type'=>'html',
        ),
        array(
            'name'=>'unit_to',
            'value'=>($model->unitTo !== null)?'<span class=label>CBelongsToRelation</span><br/>'.CHtml::link($model->unitTo->name, array('unit/view','id'=>$model->unitTo->id), array('class'=>'btn')):'n/a',
            'type'=>'html',
        ),
),
        )); ?></p>


<h2>
    Relations
</h2>

