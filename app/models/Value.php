<?php
/**
 * Class Value
 * Mongo collection to hold the user daily values for different measures
 *
 * @property float value
 * @property integer date
 * @property int unit_id
 * @property int user_id
 * @property int measure_id
 *
 * @property Unit unit
 * @property Measure measure
 * @property User user
 */
class Value extends EMongoDocument {

	public $unit;
	public $measure;
	public $user;

	public function collectionName() {
		return 'value';
	}

	public function attributeNames() {
		return [
			'value'        => Yii('model', 'Value'),
			'date'         => Yii('model', 'Date'),
			'unit_id'      => Yii('model', 'Unit #'),
			'user_id'      => Yii('model', 'User #'),
			'measure_id'   => Yii('model', 'Measure #'),

			'unit.name'    => Yii('model', 'Unit'),
			'unit.abbr'    => Yii('model', 'Unit Abbreviation'),
			'user.name'    => Yii('model', 'User'),
			'measure.name' => Yii('model', 'Measure'),
		];
	}

	public function relations() {
		return [
			'unit'    => ['one', 'Unit', 'unit_id'],
			'measure' => ['one', 'Measure', 'measure_id'],
			'user'    => ['one', 'User', 'user_id']
		];
	}

}