<?php
trait JsonApi {

	private $codes = [
		200 => 'OK',
		201 => 'Created',

		400 => 'Bad Request',
		401 => 'Unauthorized',
		403 => 'Forbidden',
		404 => 'Not Found',

		500 => 'Internal Server Error',
		501 => 'Not Implemented',
	];

	public $debugCleared = false;

	protected function clearDebug() {
		if (!$this->debugCleared) {
			if (function_exists('xdebug_disable')) xdebug_disable();
			ini_set('html_errors', 0);

			foreach (Yii::app()->log->routes as $route)
				if ($route instanceof CWebLogRoute || $route instanceof YiiDebugToolbarRoute)
					$route->enabled = false;

			$this->debugCleared = true;
		}
	}

	public function beforeAction($action) {
		$this->clearDebug();
		parent::beforeAction($action);
	}

	protected function setHTTPStatus($code) {
		header("HTTP/1.1 $code {$this->codes[$code]}");
	}

	protected function renderHTTPError($code, $message) {
		$this->setHTTPStatus($code);
		$this->renderJSON(['error' => $message]);
	}

	protected function renderJSON($data) {
		$this->layout = false;

		header('Content-type: application/json');
		echo CJSON::encode($data);

		Yii::app()->end();
	}

}