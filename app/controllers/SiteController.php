<?php

class SiteController extends Controller {

	/**
	 * Declares class-based actions.
	 */
	public function actions() {
		return array(
			'captcha' => ['class' => 'CCaptchaAction', 'backColor' => 0xFFFFFF],
			'page'    => ['class' => 'CViewAction'],
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex() {
		$measures = UserMeasure::model()
			->with('measure', 'unit')
			->findAllByAttributes(['user_id' => Yii::app()->user->id]);

		$values   = [];
		foreach ($measures as $measure) {
			$val             = new Value;
			$val->measure_id = $measure->measure_id;
			$val->unit_id    = $measure->unit_id;
			$val->user_id    = $measure->user_id;
			$val->measure    = $measure->measure;
			$val->unit       = $measure->unit;
			$values[] = $val;
		}

		$this->render('index', compact('values'));
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError() {
		if ($error = Yii::app()->errorHandler->error) {
			if (Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
		else {
			$error['message'] = "Precondition Failed";
			$error['code']    = 412;
			$this->render('error', $error);
		}
	}

	/**
	 * Login page handled via vendor module mishamx.yii-user
	 */

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout() {
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
}