<?php

/**
 * This is the model base class for the table "measure".
 *
 * Columns in table "measure" available as properties of the model:
 * @property string $id
 * @property string $name
 * @property string $unitGroup_id
 *
 * Relations of table "measure" available as properties of the model:
 * @property UnitGroup $unitGroup
 * @property User[] $users
 */
class Measure extends CActiveRecord {

	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'measure';
	}

	public function rules() {
		return array(
			array('name, unitGroup_id', 'required'),
			array('name', 'length', 'max' => 50),
			array('unitGroup_id', 'length', 'max' => 10),
			array('id, name, unitGroup_id', 'safe', 'on' => 'search'),
		);
	}

	public function relations() {
		return array(
			'unitGroup' => array(self::BELONGS_TO, 'UnitGroup', 'unitGroup_id'),
			'users'     => array(self::MANY_MANY, 'User', 'userMeasure(measure_id, user_id)'),
		);
	}

	public function attributeLabels() {
		return array(
			'id'           => Yii::t('app', 'ID'),
			'name'         => Yii::t('app', 'Name'),
			'unitGroup_id' => Yii::t('app', 'Unit Group'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id, true);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('unitGroup_id', $this->unitGroup_id);

		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
		));
	}

	public function behaviors() {
		return array(
			'CSaveRelationsBehavior', array(
				'class' => 'CSaveRelationsBehavior'
			),
		);
	}

	public function beforeValidate() {
		return parent::beforeValidate();
	}

	public function afterValidate() {
		return parent::afterValidate();
	}

	public function beforeSave() {
		return parent::beforeSave();
	}

	public function afterSave() {
		return parent::afterSave();
	}

	public function __toString() {
		return (string)$this->name;
	}


}
