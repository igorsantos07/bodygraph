<?php

/**
 * This is the model base class for the table "userMeasure".
 *
 * Columns in table "userMeasure" available as properties of the model:
 * @property integer $user_id
 * @property string $measure_id
 * @property string $unit_id
 *
 * There are no model relations.
 */
class UserMeasure extends CActiveRecord {

	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'userMeasure';
	}

	public function rules() {
		return array(
			array('user_id, measure_id, unit_id', 'required'),
			array('user_id', 'numerical', 'integerOnly' => true),
			array('measure_id, unit_id', 'length', 'max' => 10),
			array('user_id, measure_id, unit_id', 'safe', 'on' => 'search'),
		);
	}

	public function relations() {
		return [
			'user'    => [self::BELONGS_TO, 'User', 'user_id'],
			'measure' => [self::BELONGS_TO, 'Measure', 'measure_id'],
			'unit'    => [self::BELONGS_TO, 'Unit', 'unit_id']
		];
	}

	public function attributeLabels() {
		return array(
			'user_id'    => Yii::t('app', 'User'),
			'measure_id' => Yii::t('app', 'Measure'),
			'unit_id'    => Yii::t('app', 'Unit'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('user_id', $this->user_id);
		$criteria->compare('measure_id', $this->measure_id);
		$criteria->compare('unit_id', $this->unit_id);

		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
		));
	}

	public function behaviors() {
		return array(
			'CSaveRelationsBehavior', array(
				'class' => 'CSaveRelationsBehavior'
			),
			'OwnerBehavior' => array(
				'class'       => 'OwnerBehavior',
				'ownerColumn' => 'user_id',
			),

		);
	}

	public function beforeValidate() {
		return parent::beforeValidate();
	}

	public function afterValidate() {
		return parent::afterValidate();
	}

	public function beforeSave() {
		return parent::beforeSave();
	}

	public function afterSave() {
		return parent::afterSave();
	}

	public function __toString() {
		return "$this->measure ({$this->unit->abbr})";
	}


}
