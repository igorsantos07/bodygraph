<?php

/**
 * This is the model base class for the table "conversionIndex".
 *
 * Columns in table "conversionIndex" available as properties of the model:
 * @property string $id
 * @property string $index
 * @property string $unit_from
 * @property string $unit_to
 *
 * Relations of table "conversionIndex" available as properties of the model:
 * @property Unit $unitTo
 * @property Unit $unitFrom
 */
class ConversionIndex extends CActiveRecord {

	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'conversionIndex';
	}

	public function rules() {
		return array(
			array('index, unit_from, unit_to', 'required'),
			array('index', 'length', 'max' => 7),
			array('unit_from, unit_to', 'length', 'max' => 10),
			array('unit_from', 'compare', 'compareAttribute' => 'unit_to', 'operator' => '!=', 'message' => Yii::t('app', 'The two units must be different.')),
			array('id, index, unit_from, unit_to', 'safe', 'on' => 'search'),
		);
	}

	public function relations() {
		return array(
			'unitTo'   => array(self::BELONGS_TO, 'Unit', 'unit_to'),
			'unitFrom' => array(self::BELONGS_TO, 'Unit', 'unit_from'),
		);
	}

	public function attributeLabels() {
		return array(
			'id'        => Yii::t('app', 'ID'),
			'index'     => Yii::t('app', 'Index'),
			'unit_from' => Yii::t('app', 'Unit From'),
			'unit_to'   => Yii::t('app', 'Unit To'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id, true);
		$criteria->compare('index', $this->index, true);
		$criteria->compare('unit_from', $this->unit_from);
		$criteria->compare('unit_to', $this->unit_to);

		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
		));
	}

	public function behaviors() {
		return array(
			'CSaveRelationsBehavior', array(
				'class' => 'CSaveRelationsBehavior'
			),
		);
	}

	public function beforeValidate() {
		return parent::beforeValidate();
	}

	public function afterValidate() {
		return parent::afterValidate();
	}

	public function beforeSave() {
		return parent::beforeSave();
	}

	public function afterSave() {
		return parent::afterSave();
	}

	public function __toString() {
		return "$this->unitFrom to $this->unitTo";
	}


}
