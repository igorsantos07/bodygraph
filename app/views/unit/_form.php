<div class="form">
	<p class="note">
		<?php echo Yii::t('crud', 'Fields with');?> <span
			class="required">*</span> <?php echo Yii::t('crud', 'are required');?>        .
	</p>


	<?php
	$this->widget('echosen.EChosen',
		array('target' => 'select')
	);
	?>    <?php
	$form = $this->beginWidget('CActiveForm', array(
		'id'                     => 'unit-form',
		'enableAjaxValidation'   => true,
		'enableClientValidation' => true,
	));

	echo $form->errorSummary($model);
	?>

	<div class="row">
		<?php echo $form->labelEx($model, 'name'); ?>

		<?php echo $form->textField($model, 'name', array('size' => 50, 'maxlength' => 50)); ?>
		<?php echo $form->error($model, 'name'); ?>
		<?php if ('help.name' != $help = Yii::t('crud', 'help.name')) {
			echo "<span class='help-block'>$help</span>";
		} ?></div>

	<div class="row">
		<?php echo $form->labelEx($model, 'abbr'); ?>
		<?php echo $form->textField($model, 'abbr', array('size' => 10, 'maxlength' => 10)); ?>
		<?php echo $form->error($model, 'abbr'); ?>
		<?php if ('help.abbr' != $help = Yii::t('crud', 'help.abbr')) {
			echo "<span class='help-block'>$help</span>";
		} ?></div>

	<div class="row">
		<label for="unitGroup"><?php echo Yii::t('crud', 'UnitGroup'); ?></label>
		<?php $this->widget(
			'Relation',
			array(
				'model'       => $model,
				'relation'    => 'unitGroup',
				'fields'      => 'name',
				'allowEmpty'  => false,
				'style'       => 'dropdownlist',
				'htmlOptions' => array(
					'checkAll' => 'all'
				),
			)
		); ?><br />
	</div>


</div> <!-- form -->
<div class="form-actions">

	<?php
	echo CHtml::Button(Yii::t('crud', 'Cancel'), array(
		'submit' => (isset($_GET['returnUrl'])) ? $_GET['returnUrl'] : array('unit/admin'),
		'class'  => 'btn'
	));
	echo ' '.CHtml::submitButton(Yii::t('crud', 'Save'), array(
		'class' => 'btn btn-primary'
	));
	$this->endWidget(); ?>
</div>
