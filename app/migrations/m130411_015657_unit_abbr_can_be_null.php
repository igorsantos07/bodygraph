<?php

class m130411_015657_unit_abbr_can_be_null extends EDbMigration {

	public function up() {
		$this->alterColumn('unit', 'abbr', 'VARCHAR(10) NULL');
	}

	public function down() {
		$this->alterColumn('unit', 'abbr', 'VARCHAR(10) NOT NULL');
	}
}