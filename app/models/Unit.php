<?php

/**
 * This is the model base class for the table "unit".
 *
 * Columns in table "unit" available as properties of the model:
 * @property string $id
 * @property string $name
 * @property string $abbr
 * @property string $unitGroup_id
 *
 * Relations of table "unit" available as properties of the model:
 * @property ConversionIndex[] $conversionIndexes
 * @property ConversionIndex[] $conversionIndexes1
 * @property UnitGroup $unitGroup
 */
class Unit extends CActiveRecord {

	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'unit';
	}

	public function rules() {
		return array(
			array('name, unitGroup_id', 'required'),
			array('name', 'length', 'max' => 50),
			array('abbr, unitGroup_id', 'length', 'max' => 10),
			array('id, name, abbr, unitGroup_id', 'safe', 'on' => 'search'),
		);
	}

	public function relations() {
		return array(
			'conversionIndexes'  => array(self::HAS_MANY, 'ConversionIndex', 'unit_to'),
			'conversionIndexes1' => array(self::HAS_MANY, 'ConversionIndex', 'unit_from'),
			'unitGroup'          => array(self::BELONGS_TO, 'UnitGroup', 'unitGroup_id'),
		);
	}

	public function attributeLabels() {
		return array(
			'id'           => Yii::t('app', 'ID'),
			'name'         => Yii::t('app', 'Name'),
			'abbr'         => Yii::t('app', 'Abbreviation'),
			'unitGroup_id' => Yii::t('app', 'Unit Group'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id, true);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('abbr', $this->abbr, true);
		$criteria->compare('unitGroup_id', $this->unitGroup_id);

		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
		));
	}

	public function behaviors() {
		return array(
			'CSaveRelationsBehavior', array(
				'class' => 'CSaveRelationsBehavior'
			),
		);
	}

	public function getIdPlusAbbr() {
		return "{$this->id}-{$this->abbr}";
	}

	public function __toString() {
		return "$this->name ($this->abbr)";
	}


}
