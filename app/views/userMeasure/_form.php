<div class="measure-box measure-form">
	<div class="measure-content">
		<h4>Add a new measure</h4>
		<?
		$this->widget('echosen.EChosen');

		$form = $this->beginWidget('TbActiveForm', [
			'id'                     => 'user-measure-form',
			'enableAjaxValidation'   => true,
			'enableClientValidation' => true,
			'type'                   => 'inline'
		]);

		$this->widget('Relation', [
			'model'       => $new_measure,
			'relation'    => 'measure',
			'fields'      => 'name',
			'allowEmpty'  => false,
			'style'       => 'dropdownlist',
			'htmlOptions' => ['checkAll' => 'all'],
		]);

		$this->widget('Relation', [
			'model'       => $new_measure,
			'relation'    => 'unit',
			'fields'      => 'name',
			'allowEmpty'  => false,
			'style'       => 'dropdownlist',
			'htmlOptions' => ['checkAll' => 'all'],
		]);

		$this->widget('TbButton', [
			'buttonType'  => 'ajaxSubmit',
			'type'        => 'primary',
			'block'       => true,
			'size'        => 'large',
			'icon'        => 'plus',
			'label'       => 'Add measure',
			'url'         => $this->createUrl('userMeasure/add'),
			'ajaxOptions' => [
				'success' => 'function(data) {
					var $data = $(data)
					$data.css({ display: "none" })
					$("#measures .measure-form").before($data)
					$data.fadeIn()
				}',
				'error'   => "function(error) {
				alertify.error('Oops! It didn\'t work :( Can you try again later?')
				console.log(error.responseText)
			}"
			]
		]);

		$this->endWidget();
		?>
	</div>
</div>