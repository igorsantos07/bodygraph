<div class="wide form">

    <?php $form=$this->beginWidget('CActiveForm', array(
        'action'=>Yii::app()->createUrl($this->route),
        'method'=>'get',
)); ?>

                    <div class="row">
            <?php echo $form->label($model,'id'); ?>
                            <?php echo $form->textField($model,'id',array('size'=>10,'maxlength'=>10)); ?>
                    </div>

                    <div class="row">
            <?php echo $form->label($model,'index'); ?>
                            <?php echo $form->textField($model,'index',array('size'=>7,'maxlength'=>7)); ?>
                    </div>

                    <div class="row">
            <?php echo $form->label($model,'unit_from'); ?>
                            <?php echo $form->dropDownList($model,'unit_from',CHtml::listData(Unit::model()->findAll(), 'id', 'name'),array('prompt'=>'all')); ?>
                    </div>

                    <div class="row">
            <?php echo $form->label($model,'unit_to'); ?>
                            <?php echo $form->dropDownList($model,'unit_to',CHtml::listData(Unit::model()->findAll(), 'id', 'name'),array('prompt'=>'all')); ?>
                    </div>

        <div class="row buttons">
        <?php echo CHtml::submitButton(Yii::t('crud', 'Search')); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->
