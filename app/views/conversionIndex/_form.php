<div class="form">
	<p class="note">
		<?=t('Fields with <span	class="required">*</span> are required');?>        .
	</p>


	<?php
	$this->widget('echosen.EChosen');

	$form = $this->beginWidget('TbActiveForm', array(
		'id'                   => 'conversion-index-form',
		'enableAjaxValidation' => true,
//		'enableClientValidation' => true,
	));

	echo $form->errorSummary($model);
	?>

	<div class="row">
		<?=$form->labelEx($model, 'unit_from')?>
		<?php $this->widget('Relation', [
			'model'       => $model,
			'relation'    => 'unitFrom',
			'fields'      => ['name', 'abbr'],
			'relatedPk'   => 'idPlusAbbr',
			'delimiter'   => ', ',
			'allowEmpty'  => false,
			'style'       => 'dropdownlist',
			'htmlOptions' => ['checkAll' => 'all']
		]); ?>
	</div>

	<div class="row">
		<?=$form->labelEx($model, 'unit_to')?>
		<?php $this->widget('Relation', [
			'model'       => $model,
			'relation'    => 'unitTo',
			'fields'      => ['name', 'abbr'],
			'relatedPk'   => 'idPlusAbbr',
			'delimiter'   => ', ',
			'allowEmpty'  => false,
			'style'       => 'dropdownlist',
			'htmlOptions' => ['checkAll' => 'all']
		]); ?>
	</div>

	<?php
	ob_start();
	$this->widget('TbButton', [
		'label'       => 'Get from W|A',
		'htmlOptions' => [
			'id'                => 'load-conversion',
			'class'             => 'wolfram-alpha',
			'data-loading-text' => 'Loading...',
		]
	]);
	$wa_button = ob_get_clean();

	echo $form->textFieldRow($model, 'index', [
		'class'       => 'input-small',
		'size'        => 7,
		'maxlength'   => 7,
		'append'      => $wa_button,
		'hint'        => '',
		'hintOptions' => ['class' => 'help-inline', 'id' => 'wolfram-desc'],
	]) ?>

</div> <!-- form -->
<div class="form-actions">
	<?php
	echo CHtml::Button(Yii::t('crud', 'Cancel'), [
		'submit' => isset($_GET['returnUrl']) ? $_GET['returnUrl'] : ['conversionindex/admin'],
		'class'  => 'btn'
	]);
	echo ' '.CHtml::submitButton(Yii::t('crud', 'Save'), ['class' => 'btn btn-primary']);
	$this->endWidget(); ?>
</div>

<?
$get_from_wolfram_url = $this->createUrl('/conversionIndex/fromWolfram');
Yii::app()->getClientScript()->registerScript('loadFromWolfram', <<<JS
	var wabutton = $('#load-conversion'),
		wadesc = $('#wolfram-desc'),
		wainput = $(wabutton.siblings('input')[0])
	wabutton.click(function() {
		var from_val = $('#ConversionIndex_unit_from').val(),
			to_val   = $('#ConversionIndex_unit_to').val(),
			from     = from_val.substr(from_val.indexOf('-') + 1),
			to       = to_val.substr(to_val.indexOf('-') + 1)

		wabutton.button('loading')
		wainput.val('')
		wadesc.html('...')
		
		$.ajax('$get_from_wolfram_url', {
			type: 'GET',
			dataType: 'json',
			data: { from: from, to: to }
		})
		.done(function(data) {
			wadesc.html('Desc: '+data.description)
			wainput.val(data.value)
		})
		.fail({$this->default_js_error_func})
		.always(function() {
			wabutton.button('reset')
		})
	})
JS
);