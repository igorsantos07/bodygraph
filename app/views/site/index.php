<div id="home">
	<div id="buttons">
		<button class="btn btn-info btn-large">Add new measures</button>
	</div>

	<div id="new-values">
		<?=$this->renderPartial('_new_values', compact('values'))?>
	</div>
	<div id="graph">
		<?php $this->widget('vendor.yiiextensions.highsoft.HighsoftWidget', [
			'type' => 'chart',
			'options' => [
				'title' => ['text'=> 'Fruit consumption'],
				'xAxis' => [
					'categories' => ['Apples', 'Bananas', 'Oranges']
				],
			    'yAxis' => [
					'title' => ['text' => 'Fruit eaten']
				],
				'series' => [
					['name' => 'Jane', 'data' => [1,0,4]],
					['name' => 'John', 'data' => [5,7,3]],
				]
			]
		]) ?>
	</div>
</div>

<script>
	$('#buttons button').click(function() {
		$('#buttons').slideUp('fast', function() {
			$('#new-values').slideDown('fast')
		})
	})
</script>