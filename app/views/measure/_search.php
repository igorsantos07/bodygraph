<div class="wide form">

    <?php $form=$this->beginWidget('CActiveForm', array(
        'action'=>Yii::app()->createUrl($this->route),
        'method'=>'get',
)); ?>

                    <div class="row">
            <?php echo $form->label($model,'id'); ?>
                            <?php echo $form->textField($model,'id',array('size'=>10,'maxlength'=>10)); ?>
                    </div>

                    <div class="row">
            <?php echo $form->label($model,'name'); ?>
                            <?php echo $form->textField($model,'name',array('size'=>50,'maxlength'=>50)); ?>
                    </div>

                    <div class="row">
            <?php echo $form->label($model,'unitGroup_id'); ?>
                            <?php echo $form->dropDownList($model,'unitGroup_id',CHtml::listData(UnitGroup::model()->findAll(), 'id', 'name'),array('prompt'=>'all')); ?>
                    </div>

        <div class="row buttons">
        <?php echo CHtml::submitButton(Yii::t('crud', 'Search')); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- search-form -->
