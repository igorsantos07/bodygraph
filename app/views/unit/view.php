<?php
$this->breadcrumbs['Units'] = array('admin');
$this->breadcrumbs[] = $model->id;
?>
<?php $this->widget("TbBreadcrumbs", array("links"=>$this->breadcrumbs)) ?>
<h1>
    Unit <small>View #<?php echo $model->id ?></small></h1>



<?php $this->renderPartial("_toolbar", array("model"=>$model)); ?>

<h2>
    Data
</h2>

<p>
    <?php
    $this->widget('TbDetailView', array(
    'data'=>$model,
    'attributes'=>array(
            'id',
        'name',
        'abbr',
        array(
            'name'=>'unitGroup_id',
            'value'=>($model->unitGroup !== null)?'<span class=label>CBelongsToRelation</span><br/>'.CHtml::link($model->unitGroup->name, array('unitGroup/view','id'=>$model->unitGroup->id), array('class'=>'btn')):'n/a',
            'type'=>'html',
        ),
),
        )); ?></p>


<h2>
    Relations
</h2>

<div class='well'>
    <div class='row'>
<div class='span3'><?php $this->widget('bootstrap.widgets.TbButtonGroup', array(
        'type'=>'', // '', 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
        'buttons'=>array(
            array('label'=>'conversionIndexes', 'icon'=>'icon-list-alt', 'url'=> array('conversionIndex/admin')),
                array('icon'=>'icon-plus', 'url'=>array('conversionIndex/create', 'ConversionIndex' => array('unit_to'=>$model->{$model->tableSchema->primaryKey}))),
        ),
    )); ?></div><div class='span8'>
<?php
    echo '<span class=label>CHasManyRelation</span>';
    if (is_array($model->conversionIndexes)) {

        echo CHtml::openTag('ul');
            foreach($model->conversionIndexes as $relatedModel) {

                echo '<li>';
                echo CHtml::link($relatedModel->index, array('conversionIndex/view','id'=>$relatedModel->id), array('class'=>''));

                echo '</li>';
            }
        echo CHtml::closeTag('ul');
    }
?></div>
     </div> <!-- row -->
</div> <!-- well -->
<div class='well'>
    <div class='row'>
<div class='span3'><?php $this->widget('bootstrap.widgets.TbButtonGroup', array(
        'type'=>'', // '', 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
        'buttons'=>array(
            array('label'=>'conversionIndexes1', 'icon'=>'icon-list-alt', 'url'=> array('conversionIndex/admin')),
                array('icon'=>'icon-plus', 'url'=>array('conversionIndex/create', 'ConversionIndex' => array('unit_from'=>$model->{$model->tableSchema->primaryKey}))),
        ),
    )); ?></div><div class='span8'>
<?php
    echo '<span class=label>CHasManyRelation</span>';
    if (is_array($model->conversionIndexes1)) {

        echo CHtml::openTag('ul');
            foreach($model->conversionIndexes1 as $relatedModel) {

                echo '<li>';
                echo CHtml::link($relatedModel->index, array('conversionIndex/view','id'=>$relatedModel->id), array('class'=>''));

                echo '</li>';
            }
        echo CHtml::closeTag('ul');
    }
?></div>
     </div> <!-- row -->
</div> <!-- well -->
