<?php

/**
 * Phundament 3 Console Config File
 * Containes predefined yiic console commands for Phundament.
 * Define composer hooks by the following name schema: <vendor>/<packageName>-<action>

 */
$mainConfig = require('main.php');

return [
	'aliases'    => [
		'vendor'  => dirname(__FILE__).'/../../vendor',
		'webroot' => dirname(__FILE__).'/../../www',
	],
	'basePath'   => dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'       => 'BodyGraph Console Application',
	'components' => CMap::mergeArray(
		$mainConfig['components'],
		[
			'db-test' => [
				'class'            => 'CDbConnection',
				'tablePrefix'      => 'usr_',
				'connectionString' => 'sqlite:'.$applicationDirectory.'/data/test.db',
			]
		]
	),
	'modules'    => $mainConfig['modules'],
	'commandMap' => [
		// dev command
		'database'    => [
			'class' => 'vendor.schmunk42.database-command.EDatabaseCommand',
		],
		// composer callback
		'migrate'     => [
			// alias of the path where you extracted the zip file
			'class'                 => 'vendor.yiiext.migrate-command.EMigrateCommand',
			// this is the path where you want your core application migrations to be created
			'migrationPath'         => 'application.migrations',
			// the name of the table created in your database to save versioning information
			'migrationTable'        => 'migration',
			// the application migrations are in a pseudo-module called "core" by default
			'applicationModuleName' => 'core',
			// define all available modules (if you do not set this, modules will be set from yii app config)
			'modulePaths'           => [
				'rights'         => 'vendor.phundament.p3admin.modules-install.rights.migrations',
				'user'           => 'application.modules.user.migrations',
				'p3pages'        => 'vendor.phundament.p3pages.migrations',
				'p3pages-demo'   => 'vendor.phundament.p3pages.migrations-demo',
				'p3widgets'      => 'vendor.phundament.p3widgets.migrations',
				'p3widgets-demo' => 'vendor.phundament.p3widgets.migrations-demo',
				'p3media'        => 'vendor.phundament.p3media.migrations',
			],
			// you can customize the modules migrations subdirectory which is used when you are using yii module config
			'migrationSubPath'      => 'migrations',
			// here you can configure which modules should be active, you can disable a module by adding its name to this array
			'disabledModules'       => [
				'p3pages-demo', 'p3widgets-demo', // ...
			],
			// the name of the application component that should be used to connect to the database
			'connectionID'          => 'db',
			// alias of the template file used to create new migrations
			#'templateFile' => 'system.cli.migration_template',
		],
		// composer callback
		'p3bootstrap' => [
			'class'           => 'vendor.phundament.p3bootstrap.commands.P3BootstrapCommand',
			'themePath'       => 'application.themes',
			'publicThemePath' => 'webroot.themes',
		],
		// composer callback
		'p3media'     => [
			'class' => 'vendor.phundament.p3media.commands.P3MediaCommand',
		],
		// media file sync
		'rsync'       => [
			'class'   => 'vendor.phundament.p3extensions.commands.P3RsyncCommand',
			'servers' => [
				'dev'  => realpath(dirname(__FILE__).'/..'),
				'prod' => 'user@example.com:/path/to/phundament/app',
			],
			'aliases' => [
				'p3media' => 'application.data.p3media' # Note: This setting syncs P3Media Files
			],
			#'params' => '--rsh="ssh -p222"',
		],
		// composer callback
		'webapp'      => [
			'class' => 'application.commands.P3WebAppCommand',
		],

	],
	'params'     => CMap::mergeArray( $mainConfig['params'], [
		'composer.callbacks' => [
//			'post-update'                    => ['yiic', 'migrate', '--interactive=0'],
//			'post-install'                   => ['yiic', 'migrate', '--interactive=0'],
			'yiisoft/yii-install'            => ['yiic', 'webapp', 'create',
				realpath(dirname(__FILE__).'/../../'),
				'git',
				'--interactive=0'],
			'phundament/p3bootstrap-install' => ['yiic', 'p3bootstrap'],
			'phundament/p3media-install'     => ['yiic', 'p3media'],
		]
	])
];