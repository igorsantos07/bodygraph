<?php
if (!defined('DEFAULT_I18N_CATEGORY'))
	define('DEFAULT_I18N_CATEGORY', 'app');

/**
 * Makes it easier to call Yii::t() for a default message category.
 * One can override the {@link DEFAULT_I18N_CATEGORY} constant before including this file.
 * @param string $key [optional] The i18n catalog name, or the key in {@link DEFAULT_I18N_CATEGORY}
 * @param string $string The message to be translated
 * @param array $params parameters to be applied to the message using strtr. The first parameter can be a number without
 *  key. And in this case, the method will call CChoiceFormat::format to choose an appropriate message translation.
 *  Starting from version 1.1.6 you can pass parameter for CChoiceFormat::format or plural forms format without wrapping
 *  it with array. This parameter is then available as {n} in the message translation string.
 * @param array $source which message source application component to use. Defaults to null, meaning using
 *  'coreMessages' for messages belonging to the 'yii' category and using 'messages' for the rest messages.
 * @param array $language the target language. If null (default), the application language will be used.
 * @return string i18n string
 */
function t() {
	$fargc = func_num_args();
	$fargv = func_get_args();

	switch($fargc) {
		case 0: return null;
		case 1:
			$parts = [DEFAULT_I18N_CATEGORY, $fargv[0], [], null, null];
		break;

		case 2:
		case 3:
		case 4:
		case 5:
			$walk = is_array($fargv[1])? 0 : 1;
			$source = ($fargc >= (3+$walk))? $fargv[2+$walk] : null;
			$lang   = ($fargc >= (4+$walk))? $fargv[3+$walk] : null;
			$parts = [($walk? $fargv[0] : DEFAULT_I18N_CATEGORY), $fargv[0+$walk], $fargv[1+$walk], $source, $lang];
		break;
	}

	list($cat, $string, $params, $source, $lang) = $parts;

	return Yii::t($cat, $string, $params, $source, $lang);
}