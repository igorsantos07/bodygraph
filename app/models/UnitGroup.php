<?php

/**
 * This is the model base class for the table "unitGroup".
 *
 * Columns in table "unitGroup" available as properties of the model:
 * @property string $id
 * @property string $name
 *
 * Relations of table "unitGroup" available as properties of the model:
 * @property Measure[] $measures
 * @property Unit[] $units
 */
class UnitGroup extends CActiveRecord {

	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'unitGroup';
	}

	public function rules() {
		return array(
			array('name', 'required'),
			array('name', 'length', 'max' => 50),
			array('id, name', 'safe', 'on' => 'search'),
		);
	}

	public function relations() {
		return array(
			'measures' => array(self::HAS_MANY, 'Measure', 'unitGroup_id'),
			'units'    => array(self::HAS_MANY, 'Unit', 'unitGroup_id'),
		);
	}

	public function attributeLabels() {
		return array(
			'id'   => Yii::t('app', 'ID'),
			'name' => Yii::t('app', 'Name'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id, true);
		$criteria->compare('name', $this->name, true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
		));
	}

	public function behaviors() {
		return array(
			'CSaveRelationsBehavior', array(
				'class' => 'CSaveRelationsBehavior'
			),
		);
	}

	public function beforeValidate() {
		return parent::beforeValidate();
	}

	public function afterValidate() {
		return parent::afterValidate();
	}

	public function beforeSave() {
		return parent::beforeSave();
	}

	public function afterSave() {
		return parent::afterSave();
	}

	public function __toString() {
		return (string)$this->name;
	}


}
