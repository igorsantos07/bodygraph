<?php

class UserMeasureController extends Controller {

	#public $layout='//layouts/column2';
	public $scenario = "crud";

	public function filters() {
		return ['accessControl'];
	}

	public function accessRules() {
		return [
			[
				'allow',
				'actions' => ['index', 'add', 'edit', 'delete'],
				'roles'   => ['admin'],
			],
			['deny', 'users' => ['*']],
		];
	}

	public function actionIndex() {
		$measures = UserMeasure::model()->with('measure')->findAllByAttributes(['user_id' => Yii::app()->user->id]);
		$new_measure = new UserMeasure;
		$this->render('index', compact('measures', 'new_measure'));
	}

	public function actionAdd() {
		if (isset($_POST['UserMeasure'])) {
			$measure = new UserMeasure;
			$measure->attributes = $_POST['UserMeasure'];

			try {
				if ($measure->save())
					$this->renderPartial('_view', compact('measure'));
			}
			catch (Exception $e) {
				throw new CHttpException(400, $e->getMessage());
			}
		}
		else {
			throw new CHttpException(400, t('This page should be requested only to save new user measure records.'));
		}
	}

	public function actionEdit() {
		$this->render('edit');
	}

	public function actionDelete() {
		if (Yii::app()->request->isDeleteRequest && empty($_GET['id']) && empty($_POST['id'])) {
			parse_str(file_get_contents('php://input'));

			try {
				UserMeasure::model()->deleteByPk(['user_id' => Yii::app()->user->id, 'measure_id' => $id]);
			}
			catch (Exception $e) {
				throw new CHttpException(500, $e->getMessage());
			}
		} else
			throw new CHttpException(400, t('Invalid request. Please do not repeat this request again.'));
	}
}