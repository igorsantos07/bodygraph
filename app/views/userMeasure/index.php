<?php
$this->breadcrumbs = [
	'Configure my measures',
];
?>

<h1>
	<?=t('My measures')?>
	<small><?=t('Configuration')?></small>
</h1>

<div id="measures">
	<? foreach ($measures as $measure): ?>
		<?=$this->renderPartial('_view', compact('measure'))?>
	<? endforeach ?>

	<?=$this->renderPartial('_form', compact('new_measure'))?>
</div>