<?php

$applicationDirectory = realpath(dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR);
$baseUrl              = (dirname($_SERVER['SCRIPT_NAME']) != '/') ? dirname($_SERVER['SCRIPT_NAME']) : '';

$cred_file   = (isset($_ENV['CRED_FILE'])) ? $_ENV['CRED_FILE'] :
	dirname(__FILE__).DIRECTORY_SEPARATOR.'credentials.json';
$credentials = json_decode(file_get_contents($cred_file), true);

$config = [
	'basePath'   => $applicationDirectory,
	'name'       => 'BodyGraph',
	'language'   => 'en', // default language, see also components.langHandler and the login in the bottom of the file

	'preload'    => [
		'log',
		'langHandler',
		'bootstrap',
	],

	'aliases'    => [
		// composer
		'root'                           => $applicationDirectory.'/..',
		'vendor'                         => $applicationDirectory.'/../vendor',
		'bootstrap'                      => 'vendor.crisu83.yii-bootstrap',
		// p3widgets
		'jsonEditorView'                 => 'vendor.phundament.p3extensions.widgets.jsonEditorView',
		'ckeditor'                       => 'vendor.phundament.p3extensions.widgets.ckeditor',
		// p3media
		'jquery-file-upload'             => 'vendor.phundament.jquery-file-upload',
		'jquery-file-upload-widget'      => 'vendor.phundament.p3extensions.widgets.jquery-file-upload',

		// fixing 'hardcoded aliases' from extension (note: you have to use the full path)
		'ext.editable.assets.js.locales' => 'vendor.vitalets.yii-bootstrap-editable.assets.js.locales',
		'ext.editable.assets'            => 'vendor.vitalets.yii-bootstrap-editable.assets',
		'echosen'                        => 'vendor.ifdattic.echosen',
		'echosen.EChosen'                => 'vendor.ifdattic.echosen.EChosen',
		'ext.EChosen'                    => 'vendor.ifdattic.echosen',
	],

	'import'     => [
		'application.models.*',
		'application.components.*',
		'zii.widgets.*',
		'vendor.phundament.gii-template-collection.components.*', // Relation Widget
		'vendor.phundament.p3widgets.components.*', // P3WidgetContainer
		'vendor.phundament.p3extensions.components.*', // shared classes
		'vendor.phundament.p3extensions.behaviors.*', // shared classes
		'vendor.phundament.p3extensions.widgets.*', // shared classes
		'vendor.phundament.p3extensions.helpers.*', // shared classes - P3StringHelper
		'vendor.phundament.p3pages.models.*', // Meta description and keywords (P3Media)
		'application.modules.user.models.*', // User Model
		'application.modules.user.components.*', // User Components
		'vendor.crisu83.yii-rights.components.*', // RWebUser
		'vendor.crisu83.yii-bootstrap.widgets.*', // Bootstrap UI
		'vendor.yiiext.fancybox-widget.*', // Fancybox Widget
		'vendor.vitalets.yii-bootstrap-editable.*', // p3media
		'vendor.sammaye.mongoyii.*', // MongoYii
		'vendor.sammaye.mongoyii.validators.*', // MongoYii
		'vendor.sammaye.mongoyii.behaviors.*', // MongoYii
	],
	'modules'    => [
		'p3admin'   => [
			'class'  => 'vendor.phundament.p3admin.P3AdminModule',
			'params' => ['install' => false],
		],

		'p3widgets' => [
			'class'  => 'vendor.phundament.p3widgets.P3WidgetsModule',
			'params' => [
				'widgets' => [
					'CWidget'         => 'Basic HTML Widget',
					'TbCarousel'      => 'Bootstrap Carousel',
					'EFancyboxWidget' => 'Fancy Box',
					// use eg. $> php composer.phar require yiiext/swf-object-widget to get the
					// widget source; import widget class or set an alias.
					#'P3MarkdownWidget' => 'Markdown Widget'
					#'ESwfObjectWidget' => 'SWF Object',
				],
			],
		],

		'p3pages'   => [
			'class'  => 'vendor.phundament.p3pages.P3PagesModule',
			'params' => [
				'availableLayouts' => [
					'//layouts/main' => 'Main Layout',
					'_TbNavbar'      => '_TbNavbar (Top-Menu Container)'
				],
				'availableViews'   => [
					'//p3pages/column1' => 'One Column',
					'//p3pages/column2' => 'Two Columns',
				]
			],
		],

		'rights'    => [
			'class'        => 'vendor.crisu83.yii-rights.RightsModule',
			'appLayout'    => '//layouts/main',
			'userIdColumn' => 'id',
			'userClass'    => 'User',
			'cssFile'      => '/themes/backend/css/yii-rights.css'
			#'install' => true, // Enables the installer.
			#'superuserName' => 'admin'
		],

		'user'      => [
			'class'               => 'application.modules.user.UserModule',
			'sendActivationMail'  => false,
			'activeAfterRegister' => true,
			'autoLogin'           => true,
			'hash'                => 'sha512',
			'rememberMeTime'      => 60 * 60 * 24 * 365 * 2,
			'tableUsers'          => 'user',
			'tableProfiles'       => 'profile',
			'tableProfileFields'  => 'profileField',
		],
	],

	'components' => [
		'db'            => [
			'connectionString'      => "mysql:host={$credentials['MYSQLS']['MYSQLS_HOSTNAME']};port={$credentials['MYSQLS']['MYSQLS_PORT']};dbname={$credentials['MYSQLS']['MYSQLS_DATABASE']}",
			'username'              => $credentials['MYSQLS']['MYSQLS_USERNAME'],
			'password'              => $credentials['MYSQLS']['MYSQLS_PASSWORD'],
			'emulatePrepare'        => true,
			'charset'               => 'utf8',
			'schemaCachingDuration' => 60 * 60 * 24 * 30,
		],

		'mongodb'       => [
			'class'  => 'EMongoClient',
			'server' => $credentials['MONGOLAB']['MONGOLAB_URI'],
			'db'     => 'bodygraph'
		],

		'cache'         => [
			'class' => 'CApcCache',
		],

		'authManager'   => [
			'class'        => 'RDbAuthManager', // Provides support authorization item sorting.
			'defaultRoles' => ['Authenticated', 'Guest'], // see correspoing business rules, note: superusers always get checkAcess == true
		],

		'user'          => [
			'class'          => 'RWebUser', // crisu83/yii-rights: Allows super users access implicitly.
			'behaviors'      => ['vendor.schmunk42.web-user-behavior.WebUserBehavior'], // compatibility behavior for yii-user and yii-rights
			'allowAutoLogin' => true,
			'loginUrl'       => ['/user/login'],
		],

		'returnUrl'     => [
			'class' => 'vendor.phundament.p3extensions.components.P3ReturnUrl',
		],

		'urlManager'    => [
			'class'          => 'vendor.phundament.p3extensions.components.P3LangUrlManager',
			'showScriptName' => false,
			'appendParams'   => false, // in general more error resistant
			'urlFormat'      => 'path', // use 'path', otherwise rules below won't work
			'rules'          => [
				// disabling standard login page
				'<lang:[a-z]{2}>/site/login'             => 'user/login',
				'site/login'                             => 'user/login',
				// convenience rules
				'admin'                                  => 'p3admin',
				'<lang:[a-z]{2}>/pages/<view:\w+>'       => 'site/page',
				'<lang:[a-z]{2}>/wiki/<page:\w+>'        => 'wiki',
				// Yii
				'<controller:\w+>/<id:\d+>'              => '<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
				// general language and route handling
				'<lang:[a-z]{2}>'                        => '',
				'<lang:[a-z]{2}>/<_c>'                   => '<_c>',
				'<lang:[a-z]{2}>/<_c>/<_a>'              => '<_c>/<_a>',
				'<lang:[a-z]{2}>/<_m>/<_c>/<_a>'         => '<_m>/<_c>/<_a>',
			],
		],

		'less'          => [
			'class'   => 'vendor.crisu83.yii-less.components.Less',
			'mode'    => 'server',
			'files'   => ['less/styles.less' => 'css/styles.css'],
			'options' => [
				'compression'       => 'yui',
				'optimizationLevel' => '2',
				'compilerPath'      => 'lessc',
			]
		],

		'bootstrap'     => [
			'class'         => 'vendor.crisu83.yii-bootstrap.components.Bootstrap', // assuming you extracted bootstrap under extensions
			'coreCss'       => false, // whether to register the Bootstrap core CSS (bootstrap.min.css], defaults to true
			'responsiveCss' => false, // whether to register the Bootstrap responsive CSS (bootstrap-responsive.min.css], default to false
			'plugins'       => [
				// Optionally you can configure the "global" plugins (button, popover, tooltip and transition)
				// To prevent a plugin from being loaded set it to false as demonstrated below
				'transition' => false, // disable CSS transitions
				'tooltip'    => [
					'selector' => 'a.tooltip', // bind the plugin tooltip to anchor tags with the 'tooltip' class
					'options'  => [
						'placement' => 'bottom', // place the tooltips below instead
					],
				],
				// If you need help with configuring the plugins, please refer to Bootstrap's own documentation:
				// http://twitter.github.com/bootstrap/javascript.html
			],
		],

		'image'         => [
			'class'  => 'vendor.phundament.p3extensions.components.image.CImageComponent',
			// GD or ImageMagick
			'driver' => 'GD',
		],

		'langHandler'   => [
			'class'     => 'vendor.phundament.p3extensions.components.P3LangHandler',
			'languages' => ['en', 'pt']
		],

		'log'           => [
			'class'  => 'CLogRouter',
			'routes' => [
				[
					'class'  => 'CFileLogRoute',
					'levels' => 'error, warning',
				],
			],
		],

		'errorHandler'  => ['errorAction' => 'site/error'],

		'widgetFactory' => [
			'class'   => 'CWidgetFactory',
			'widgets' => [
				'EChosen' => ['target' => 'select']
			]
		],
	],
	// application-level parameters that can be accessed using Yii::app()->params['paramName']
	'params'     => [
		'adminEmail'          => 'igorsantos07@gmail.com',
		'p3.backendTheme'     => 'backend', // defaults to 'backend'
		'p3.fallbackLanguage' => 'en', // defaults to 'en'
		'credentials'         => $credentials,
	],
];

$localConfigFile = dirname(__FILE__).DIRECTORY_SEPARATOR.'local.php';
if (is_file($localConfigFile)) {
	$config = CMap::mergeArray($config, require($localConfigFile));
}

return $config;