<?php
class ExtraDbMigration extends EDbMigration {

	public static $defaultEngine = 'InnoDB';

	const PK = 'INT UNSIGNED NOT NULL AUTO_INCREMENT';
	const FK = 'INT UNSIGNED NOT NULL';
	const ALT_FK = 'INT(11) NOT NULL';

	public function dbType() {
		return strtok(Yii::app()->db->connectionString, ':');
	}

	/**
	 * Calls the default createTable from EDbMigration, plus:
	 * - adds the {@link $defaultEngine} if the database is MySQL
	 * - adds primary key to tables with no PK in their definition and an "id" field
	 * - adds foreign keys to to fields with the format «table»_id (does not look for existing FKs in the table!)
	 *
	 * @param string $table
	 * @param array $columns
	 * @param string $options
	 */
	public function createTable($table, $columns, $options = '') {
		if ($this->dbType() == 'mysql') {
			if (strpos($options, 'ENGINE=') !== false)
				$options .= 'ENGINE='.self::$defaultEngine;
		}

		if (array_key_exists('id', $columns) &&
			!array_reduce($columns, function ($result, $item) {
				if ($result) return $result;
				$result = strpos(strtoupper($item), 'PRIMARY') !== false;
			})
		) {
			$columns[] = 'CONSTRAINT PRIMARY KEY (id)';
		}

		$create_table = parent::createTable($table, $columns, $options);

		foreach($columns as $field => $details) {
			if (strpos($field, '_id') > 0) {
				$ref_table = strtok($field, '_');
				$this->addForeignKey("FK_{$table}_{$ref_table}", $table, $field, $ref_table, 'id');
			}
		}

		return $create_table;
	}

}