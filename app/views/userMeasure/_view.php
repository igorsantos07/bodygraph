<div class="measure-box">
	<div class="measure-content">
		<h4>
			<?=$measure->measure->name?>
			<? if ($measure->unit->abbr): ?>
				<small>(<abbr title="<?= $measure->unit->name ?>"><?=$measure->unit->abbr?></abbr>)</small>
			<? endif ?>
		</h4>
		<?
		$this->widget('TbButtonGroup', [
			'buttons' => [
				/*[
					'label'    => 'Edit',
					'type'     => 'info',
					'icon'     => 'pencil',
					'disabled' => true,
					'url'      => $this->createUrl('edit', ['id' => $measure->measure_id])
				],*/
				[
					'label'       => 'Remove',
					'type'        => 'danger',
					'icon'        => 'remove',
					'htmlOptions' => [
						'id'    => 'remove-'.$measure->measure_id,
						'class' => 'remove-measure',
					]
				]
			]
		]);

		$cs = Yii::app()->getClientScript();
		if (!$cs->isScriptRegistered('removeMeasure')) {
			$remove_url  = $this->createUrl('delete');
			$confirm_msg = t('Are you sure you want to remove this measure?<br />Your entire data about it will be erased!');
			$ok_msg      = t('Go ahead');
			$cancel_msg  = t('Cancel');
			$cs->registerScript('removeMeasure', <<<JS
			$('.remove-measure').on('click', function() {
				alertify.set({ labels: { ok: '$ok_msg', cancel: '$cancel_msg' }})
				var button = $(this),
					id = button.attr('id').substr('remove-'.length)
				alertify.confirm('$confirm_msg', function(ok) {
					if (ok) {
						$.ajax('$remove_url', {
							type: 'delete',
							data: { id: id },
							success: function(data) {
								button.parents('.measure-box').fadeOut()
							},
							error: function(error) {
								alertify.error('Oops! It didn\'t work :( Can you try again later?')
								console.log(error.responseText)
							}
						})
					}
				})
			})
JS
			);
		}
		?>
	</div>
</div>