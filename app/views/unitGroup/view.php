<?php
$this->breadcrumbs['Unit Groups'] = array('admin');
$this->breadcrumbs[] = $model->id;
?>
<?php $this->widget("TbBreadcrumbs", array("links"=>$this->breadcrumbs)) ?>
<h1>
    Unit Group <small>View #<?php echo $model->id ?></small></h1>



<?php $this->renderPartial("_toolbar", array("model"=>$model)); ?>

<h2>
    Data
</h2>

<p>
    <?php
    $this->widget('TbDetailView', array(
    'data'=>$model,
    'attributes'=>array(
            'id',
        'name',
),
        )); ?></p>


<h2>
    Relations
</h2>

<div class='well'>
    <div class='row'>
<div class='span3'><?php $this->widget('bootstrap.widgets.TbButtonGroup', array(
        'type'=>'', // '', 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
        'buttons'=>array(
            array('label'=>'measures', 'icon'=>'icon-list-alt', 'url'=> array('measure/admin')),
                array('icon'=>'icon-plus', 'url'=>array('measure/create', 'Measure' => array('unitGroup_id'=>$model->{$model->tableSchema->primaryKey}))),
        ),
    )); ?></div><div class='span8'>
<?php
    echo '<span class=label>CHasManyRelation</span>';
    if (is_array($model->measures)) {

        echo CHtml::openTag('ul');
            foreach($model->measures as $relatedModel) {

                echo '<li>';
                echo CHtml::link($relatedModel->name, array('measure/view','id'=>$relatedModel->id), array('class'=>''));

                echo '</li>';
            }
        echo CHtml::closeTag('ul');
    }
?></div>
     </div> <!-- row -->
</div> <!-- well -->
<div class='well'>
    <div class='row'>
<div class='span3'><?php $this->widget('bootstrap.widgets.TbButtonGroup', array(
        'type'=>'', // '', 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
        'buttons'=>array(
            array('label'=>'units', 'icon'=>'icon-list-alt', 'url'=> array('unit/admin')),
                array('icon'=>'icon-plus', 'url'=>array('unit/create', 'Unit' => array('unitGroup_id'=>$model->{$model->tableSchema->primaryKey}))),
        ),
    )); ?></div><div class='span8'>
<?php
    echo '<span class=label>CHasManyRelation</span>';
    if (is_array($model->units)) {

        echo CHtml::openTag('ul');
            foreach($model->units as $relatedModel) {

                echo '<li>';
                echo CHtml::link($relatedModel->name, array('unit/view','id'=>$relatedModel->id), array('class'=>''));

                echo '</li>';
            }
        echo CHtml::closeTag('ul');
    }
?></div>
     </div> <!-- row -->
</div> <!-- well -->
