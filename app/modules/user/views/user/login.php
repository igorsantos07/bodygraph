<?php
$this->pageTitle = Yii::app()->name.' - '.UserModule::t("Login");
$this->breadcrumbs = array(
	UserModule::t("Login"),
);
?>

<h1><?=UserModule::t("Login")?></h1>

<p><?=UserModule::t("Please fill out the following form with your login credentials:")?></p>
<div>
	<?php $form = $this->beginWidget('TbActiveForm', [
		'htmlOptions' => ['class' => 'well']
	]) ?>

	<p class="note"><?=UserModule::t('Fields with <span class="required">*</span> are required.')?></p>

	<div class="controls">
		<?=$form->textFieldRow($model, 'username')?>
	</div>

	<div class="controls">
		<?=$form->passwordFieldRow($model, 'password')?>
	</div>

	<div class="controls">
		<p class="hint">
			<?=CHtml::link(UserModule::t("Register"), Yii::app()->getModule('user')->registrationUrl)?>
			| <?=CHtml::link(UserModule::t("Lost Password?"), Yii::app()
				->getModule('user')->recoveryUrl)?>
		</p>
	</div>

	<div class="controls">
		<?=$form->checkboxRow($model, 'rememberMe')?>
	</div>

	<?php $this->widget('TbButton', ['buttonType' => 'submit', 'type' => 'primary', 'label' => UserModule::t("Login")])?>

	<? $this->endWidget() ?>
</div>