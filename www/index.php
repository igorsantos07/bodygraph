<?php
$yii    = dirname(__FILE__).'/../vendor/yiisoft/yii/framework/yii.php';
$config = dirname(__FILE__).'/../app/config/main.php';

if (isset($_ENV['DEP_NAME'])) {
	$env = substr(strstr($_ENV['DEP_NAME'], '/'), 1);
	switch ($env) {
		case 'default': define('ENV', 'prod');  break;
		default:        define('ENV', $env);    break;
	}
}
else {
	define('ENV', 'development');
}

define('PROD', (ENV == 'prod'));

if (PROD) {
	defined('YII_DEBUG') or define('YII_DEBUG', false);
	defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 0);
} else {
	defined('YII_DEBUG') or define('YII_DEBUG', true);
	defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);
}

require_once($yii);
$app = Yii::createWebApplication($config);
Yii::import('ext.t', true);
$app->run();