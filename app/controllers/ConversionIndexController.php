<?php
Yii::import('ext.JsonApi');

class ConversionIndexController extends Controller {

	use JsonApi {
	JsonApi::beforeAction as JSONBeforeAction;
	}

	#public $layout='//layouts/column2';
	public $defaultAction = "admin";
	public $scenario = "crud";

	public function filters() {
		return array(
			'accessControl',
		);
	}

	public function accessRules() {
		return [
			[
				'allow', 'roles'   => ['admin'],
				         'actions' => ['index', 'view', 'create', 'update', 'admin', 'delete']
			],
			['allow', 'users' => ['*'], 'actions' => ['fromWolfram']],
			['deny', 'users' => ['*']],
		];
	}

	public function beforeAction($action) {
		$this->JSONBeforeAction($action);
		// map identifcationColumn to id
		if (!isset($_GET['id']) && isset($_GET['id'])) {
			$model = ConversionIndex::model()->find('id = :id', array(
				':id' => $_GET['id']
			));
			if ($model !== null) {
				$_GET['id'] = $model->id;
			}
			else {
				throw new CHttpException(400);
			}
		}
		if ($this->module !== null) {
			$this->breadcrumbs[$this->module->Id] = array('/'.$this->module->Id);
		}
		return true;
	}

	public function actionView($id) {
		$model = $this->loadModel($id);
		$this->render('view', array('model' => $model,));
	}

	public function actionCreate() {
		$model           = new ConversionIndex;
		$model->scenario = $this->scenario;

		$this->performAjaxValidation($model, 'conversion-index-form');

		if (isset($_POST['ConversionIndex'])) {
			$model->attributes = $_POST['ConversionIndex'];

			try {
				if ($model->save()) {
					if (isset($_GET['returnUrl'])) {
						$this->redirect($_GET['returnUrl']);
					}
					else {
						$this->redirect(array('view', 'id' => $model->id));
					}
				}
			}
			catch (Exception $e) {
				$model->addError('id', $e->getMessage());
			}
		}
		elseif (isset($_GET['ConversionIndex'])) {
			$model->attributes = $_GET['ConversionIndex'];
		}

		$this->render('create', array('model' => $model));
	}

	public function actionUpdate($id) {
		$model           = $this->loadModel($id);
		$model->scenario = $this->scenario;

		$this->performAjaxValidation($model, 'conversion-index-form');

		if (isset($_POST['ConversionIndex'])) {
			$model->attributes = $_POST['ConversionIndex'];

			try {
				if ($model->save())
					$this->redirect(isset($_GET['returnUrl']) ? $_GET['returnUrl'] : ['view', 'id' => $model->id]);
			}
			catch (Exception $e) {
				$model->addError('id', $e->getMessage());
			}
		}

		$this->render('update', array('model' => $model,));
	}

	public function actionDelete($id) {
		if (!Yii::app()->request->isPostRequest)
			throw new CHttpException(400, t('Invalid request. Please do not repeat this request again.'));

		try {
			$this->loadModel($id)->delete();
		}
		catch (Exception $e) {
			throw new CHttpException(500, $e->getMessage());
		}

		if (!isset($_GET['ajax']))
			$this->redirect(isset($_GET['returnUrl']) ? $_GET['returnUrl'] : ['admin']);
	}

	public function actionIndex() {
		$this->render('index', ['dataProvider' => new CActiveDataProvider('ConversionIndex')]);
	}

	public function actionAdmin() {
		$model = new ConversionIndex('search');
		$model->unsetAttributes();

		if (isset($_GET['ConversionIndex']))
			$model->attributes = $_GET['ConversionIndex'];

		$this->render('admin', array('model' => $model,));
	}

	public function actionFromWolfram() {
		if (!isset($_GET['from']) || !isset($_GET['from']))
			$this->renderHTTPError(400, t('Missing GET arguments "{arg1}" and/or "{arg2}"', ['{arg1}' => 'from', '{arg2}' => 'to']));

		Yii::import('ext.WolframAlphaAPI', true);
		$wolfram = new WolframAlphaAPI;
		$wolfram->queryPlainText("{$_GET['from']} to {$_GET['to']}");

		$result = [
			'description' => $wolfram->getDescription()?: 'No description given',
			'value'       => (float)$wolfram->getMainResult()?: 0
			//casting something like "2.208 lb" into float left only the number, removing the letters from it
		];

		$this->renderJSON($result);
	}

	public function loadModel($id) {
		$model = ConversionIndex::model()->findByPk($id);
		if ($model === null)
			throw new CHttpException(404, Yii::t('crud', 'The requested page does not exist.'));
		return $model;
	}

	protected function performAjaxValidation($model) {
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'conversion-index-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
