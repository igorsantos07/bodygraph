<?php
$this->breadcrumbs['Measures'] = array('admin');
$this->breadcrumbs[] = $model->id;
?>
<?php $this->widget("TbBreadcrumbs", array("links"=>$this->breadcrumbs)) ?>
<h1>
    Measure <small>View #<?php echo $model->id ?></small></h1>



<?php $this->renderPartial("_toolbar", array("model"=>$model)); ?>

<h2>
    Data
</h2>

<p>
    <?php
    $this->widget('TbDetailView', array(
    'data'=>$model,
    'attributes'=>array(
            'id',
        'name',
        array(
            'name'=>'unitGroup_id',
            'value'=>($model->unitGroup !== null)?'<span class=label>CBelongsToRelation</span><br/>'.CHtml::link($model->unitGroup->name, array('unitGroup/view','id'=>$model->unitGroup->id), array('class'=>'btn')):'n/a',
            'type'=>'html',
        ),
),
        )); ?></p>


<h2>
    Relations
</h2>

<div class='well'>
    <div class='row'>
<div class='span3'><?php $this->widget('bootstrap.widgets.TbButtonGroup', array(
        'type'=>'', // '', 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
        'buttons'=>array(
            array('label'=>'users', 'icon'=>'icon-list-alt', 'url'=> array('user/admin')),
                array('icon'=>'icon-plus', 'url'=>array('user/create', 'User' => array('userMeasure(measure_id, user_id)'=>$model->{$model->tableSchema->primaryKey}))),
        ),
    )); ?></div><div class='span8'>
<?php
    echo '<span class=label>CManyManyRelation</span>';
    if (is_array($model->users)) {

        echo CHtml::openTag('ul');
            foreach($model->users as $relatedModel) {

                echo '<li>';
                echo CHtml::link($relatedModel->username, array('user/view','id'=>$relatedModel->id), array('class'=>''));

                echo '</li>';
            }
        echo CHtml::closeTag('ul');
    }
?></div>
     </div> <!-- row -->
</div> <!-- well -->
