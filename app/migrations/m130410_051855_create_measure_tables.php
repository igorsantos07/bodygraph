<?php

class m130410_051855_create_measure_tables extends ExtraDbMigration {

	public function safeUp() {
		$this->createTable('unitGroup', [
			'id'   => self::PK,
			'name' => 'VARCHAR(50) NOT NULL'
		]);
		$this->createIndex('UK_unitGroup_name', 'unitGroup', 'name', true);

		$this->createTable('unit', [
			'id'           => self::PK,
			'name'         => 'VARCHAR(50) NOT NULL',
			'abbr'         => 'VARCHAR(10) NOT NULL',
			'unitGroup_id' => self::FK
		]);
		$this->createIndex('UK_units_name_abbr', 'unit', 'name, abbr', true);

		$this->createTable('conversionIndex', [
			'id'        => self::PK,
			'index'     => 'DECIMAL(7,3) NOT NULL',
			'unit_from' => self::FK,
			'unit_to'   => self::FK,
		]);
		$this->addForeignKey('FK_conversionIndex_unit_from', 'conversionIndex', 'unit_from', 'unit', 'id');
		$this->addForeignKey('FK_conversionIndex_unit_to', 'conversionIndex', 'unit_to', 'unit', 'id');
		$this->createIndex('UK_conversionIndex_units', 'conversionIndex', 'unit_from, unit_to', true);

		$this->createTable('measure', [
			'id' => self::PK,
			'name' => 'VARCHAR(50) NOT NULL',
			'unitGroup_id' => self::FK,
		]);
		$this->createIndex('UK_measure_name', 'measure', 'name', true);

		$this->createTable('userMeasure', [
			'user_id' => self::ALT_FK,
			'measure_id' => self::FK,
			'unit_id' => self::FK,
		]);
		$this->addPrimaryKey('FK_userMeasure', 'userMeasure', 'user_id, measure_id');
	}

	public function safeDown() {
		$this->dropTable('userMeasure');
		$this->dropTable('measure');
		$this->dropTable('conversionIndex');
		$this->dropTable('unit');
		$this->dropTable('unitGroup');
	}
}