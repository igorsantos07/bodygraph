<!DOCTYPE html>
<html lang="<?= Yii::app()->language ?>">
<head>
	<meta charset="utf-8">
	<title><?=CHtml::encode($this->pageTitle)?></title>
	<meta name="description"
	      content="<?= (P3Page::getActivePage()) ? P3Page::getActivePage()->t('description') : '' ?>">
	<meta name="keywords" content="<?= (P3Page::getActivePage()) ? P3Page::getActivePage()->t('keywords') : '' ?>">
	<meta name="author" content="Igor Santos, igorsantos07">

	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="shortcut icon" href="/favicon.ico">
	<link rel="apple-touch-icon" href="/images/apple-touch-icon.png">

	<!--[if lt IE 9]>
	<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

	<?
	Yii::app()->less->register();
	$fabien_d = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('vendor.fabien-d'));
	Yii::app()->getClientScript()
//			->registerCssFile(Yii::app()->baseUrl.'/css/styles.css')
		->registerCssFile($fabien_d.'/alertify.js-shim/themes/alertify.core.css')
		->registerCssFile($fabien_d.'/alertify.js-shim/themes/alertify.bootstrap.css')
//			->registerCoreScript('jquery.ui')
		->registerScriptFile($fabien_d.'/alertify.js-shim/alertify.min.js');
	?>
</head>

<body>
<?
foreach (Yii::app()->user->getFlashes() as $type => $message)
	Yii::app()->getClientScript()->registerScript(
		"alertify_$type",
		'alertify.log("'.addslashes($message).'", "'.$type.'")',
		CClientScript::POS_END
	);

$this->renderFile(Yii::getPathOfAlias('application.views.layouts').DIRECTORY_SEPARATOR.'_menu.php') ?>

<div class="container">
	<div class="subwrapper">
		<?=$content?>
	</div>

	<hr>

	<footer>
		<span class="author"><?=Yii::t('app', 'By')?> <a href="http://www.igorsantos.com.br">Igor Santos</a></span>
		<span class="code">
			<a href="https://bitbucket.org/igorsantos07/bodygraph"><?=Yii::t('app', 'Code')?></a> &
			<a href="https://bitbucket.org/igorsantos07/bodygraph/issues?status=new&status=open"><?=Yii::t('app', 'Roadmap/Bugs')?></a>
		</span>
		<span class="software">
			<a href="http://phundament.com">Phundament 3</a> +
			<a href="http://yiiframework.com">Yii <?=Yii::getVersion()?></a>
		</span>
	</footer>
</div>
</body>
</html>