<?php
$this->breadcrumbs[] = 'Unit Groups';


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('unit-group-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<?php $this->widget("TbBreadcrumbs", array("links" => $this->breadcrumbs)) ?>
<h1>
	<?php echo Yii::t('crud', 'Unit Groups'); ?>
	<small><?php echo Yii::t('crud', 'Manage'); ?></small>
</h1>

<?php $this->renderPartial("_toolbar", array("model" => $model)); ?>
<?php $this->widget('TbGridView', [
	'id'           => 'unit-group-grid',
	'type'         => ['striped', 'hover'],
	'dataProvider' => $model->search(),
	'filter'       => $model,
	'pager'        => [
		'class'               => 'TbPager',
		'displayFirstAndLast' => true,
	],
	'columns'=> array(
	'id',
	'name',
	array(
		'class'           => 'TbButtonColumn',
		'viewButtonUrl'   => "Yii::app()->controller->createUrl('view', array('id' => \$data->id))",
		'updateButtonUrl' => "Yii::app()->controller->createUrl('update', array('id' => \$data->id))",
		'deleteButtonUrl' => "Yii::app()->controller->createUrl('delete', array('id' => \$data->id))",
	),
),]) ; ?>
