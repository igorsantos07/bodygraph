<?
$form = $this->beginWidget('TbActiveForm', [
	'type'        => 'horizontal',
	'htmlOptions' => ['class' => 'well well-small']
]);
echo '<div class="values">';
	foreach ($values as $i => $value) {
		echo '<div class="new-value">';
			echo $form->textFieldRow($value, 'value', [
				'append'       => $value->unit->abbr ? : null,
				'class'        => 'input-mini',
				'id'           => 'value-'.$i,
				'labelOptions' => [
					'label' => $value->measure->name,
					'for'   => 'value-'.$i
				]
			]);
		echo '</div>';
	}
echo '</div>';

echo '<div class="form-actions">';
	$this->widget('TbButton', ['label' => t('Cancel')]); //TODO: fazer esse botão fechar o box de volta
	$this->widget('TbButton', [
		'buttonType' => 'submit',
		'type'       => 'primary',
		'size'       => 'large',
		'label'      => t('Save')
	]);
echo '</div>';

$this->endWidget();
?>