<?php

function menuItem($label, $path, $icon = null, $visible = null) {
	$item = [
		'label' => ($label[0] == '#') ? substr($label, 1) : Yii::t('app', $label),
		'url'   => (is_array($path)) ? $path : (($path == '#') ? '#' : [$path])
	];
	if ($icon)              $item['icon']       = $icon;
	if (isset($visible))    $item['visible']    = $visible;

	return $item;
}

Yii::import('p3pages.modules.*');

$rootNode = P3Page::model()->findByAttributes(array('layout' => '_TbNavbar'));
$page     = P3Page::getActivePage();
if ($page !== null) {
	$translation = $page->getTranslationModel();
} else {
	$translation = null;
}

$this->widget('TbNavbar', [
	'brand'    => '<img src="/images/logo_32.png" /> '.Yii::app()->name,
	'collapse' => true,
	'items'    => [
		[
			'class' => 'TbMenu',
			'items' => P3Page::getMenuItems($rootNode)
		],

		//language menu
		[
			'class'       => 'TbMenu',
			'htmlOptions' => ['class' => 'pull-right'],
			'items'       => [
				[
					'label' => Yii::app()->language,
					'icon'  => 'globe white',
					'url'   => '#',
					'items' => [
						['label' => 'Choose Language'],
						menuItem('#English',    array_merge([''], $_GET, ['lang' => 'en'])),
						menuItem('#Português',  array_merge([''], $_GET, ['lang' => 'pt'])),
					],
				]
			]
		],

		//other items
		[
			'class'       => 'TbMenu',
			'htmlOptions' => ['class' => 'pull-right'],
			'items'       => [
				[
					'url'     => '#',
					'label'   => 'Admin',
					'visible' => Yii::app()->user->checkAccess('Admin'),
					'icon'    => 'cog white',
					'items'   => [
						['label' => 'Application'],
						menuItem('User',                '/user/admin/admin',    'user'),
						menuItem('User Rights',         '/rights',              'briefcase'),
						menuItem('Units',               '/unit',                'list-ol'),
						menuItem('Unit Groups',         '/unitGroup',           'list-alt'),
						menuItem('Conversion Indexes',  '/conversionIndex',     'exchange'),
						menuItem('Measures',            '/measure',             'dashboard'),
						'---',
						menuItem('Settings',            '/p3admin/default/settings', 'wrench'),
					]
				],
				[
					'label'   => Yii::app()->user->name,
					'visible' => !Yii::app()->user->isGuest,
					'icon'    => Yii::app()->user->isSuperuser ? 'warning-sign white' : 'user white',
					'items'   => [
						menuItem('View my profile',         '/user/profile',    'user'),
						menuItem('Configure my measures',   '/userMeasure',     'dashboard'),
						menuItem('Logout',                  '/site/logout',     'lock'),
					]
				],
				menuItem('Login', Yii::app()->user->loginUrl, 'lock white', Yii::app()->user->isGuest),
			],
		],
	]
]);
?>