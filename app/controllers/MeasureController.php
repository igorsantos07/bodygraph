<?php

class MeasureController extends Controller {

	#public $layout='//layouts/column2';
	public $defaultAction = "admin";
	public $scenario = "crud";

	public function filters() {
		return array(
			'accessControl',
		);
	}

	public function accessRules() {
		return array(
			array(
				'allow',
				'actions' => array('index', 'view', 'create', 'update', 'admin', 'delete'),
				'roles'   => array('admin'),
			),
			array(
				'deny',
				'users' => array('*'),
			),
		);
	}

	public function beforeAction($action) {
		parent::beforeAction($action);
		// map identifcationColumn to id
		if (!isset($_GET['id']) && isset($_GET['id'])) {
			$model = Measure::model()->find('id = :id', array(
				':id' => $_GET['id']
			));
			if ($model !== null) {
				$_GET['id'] = $model->id;
			} else {
				throw new CHttpException(400);
			}
		}
		if ($this->module !== null) {
			$this->breadcrumbs[$this->module->Id] = array('/'.$this->module->Id);
		}
		return true;
	}

	public function actionView($id) {
		$model = $this->loadModel($id);
		$this->render('view', array('model' => $model,));
	}

	public function actionCreate() {
		$model           = new Measure;
		$model->scenario = $this->scenario;

		$this->performAjaxValidation($model, 'measure-form');

		if (isset($_POST['Measure'])) {
			$model->attributes = $_POST['Measure'];

			if (isset($_POST['Measure']['User']))
				$model->setRelationRecords('users', $_POST['Measure']['User']);
			try {
				if ($model->save()) {
					if (isset($_GET['returnUrl'])) {
						$this->redirect($_GET['returnUrl']);
					} else {
						$this->redirect(array('view', 'id' => $model->id));
					}
				}
			}
			catch (Exception $e) {
				$model->addError('id', $e->getMessage());
			}
		} elseif (isset($_GET['Measure'])) {
			$model->attributes = $_GET['Measure'];
		}

		$this->render('create', array('model' => $model));
	}


	public function actionUpdate($id) {
		$model           = $this->loadModel($id);
		$model->scenario = $this->scenario;

		$this->performAjaxValidation($model, 'measure-form');

		if (isset($_POST['Measure'])) {
			$model->attributes = $_POST['Measure'];

			if (isset($_POST['Measure']['User']))
				$model->setRelationRecords('users', $_POST['Measure']['User']);
			else
				$model->setRelationRecords('users', array());

			try {
				if ($model->save()) {
					if (isset($_GET['returnUrl'])) {
						$this->redirect($_GET['returnUrl']);
					} else {
						$this->redirect(array('view', 'id' => $model->id));
					}
				}
			}
			catch (Exception $e) {
				$model->addError('id', $e->getMessage());
			}
		}

		$this->render('update', array('model' => $model,));
	}

	public function actionEditableSaver() {
		Yii::import('EditableSaver'); //or you can add import 'ext.editable.*' to config
		$es = new EditableSaver('Measure'); // classname of model to be updated
		$es->update();
	}

	public function actionDelete($id) {
		if (Yii::app()->request->isPostRequest) {
			try {
				$this->loadModel($id)->delete();
			}
			catch (Exception $e) {
				throw new CHttpException(500, $e->getMessage());
			}

			if (!isset($_GET['ajax'])) {
				if (isset($_GET['returnUrl'])) {
					$this->redirect($_GET['returnUrl']);
				} else {
					$this->redirect(array('admin'));
				}
			}
		} else
			throw new CHttpException(400, Yii::t('crud', 'Invalid request. Please do not repeat this request again.'));
	}

	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('Measure');
		$this->render('index', array('dataProvider' => $dataProvider,));
	}

	public function actionAdmin() {
		$model = new Measure('search');
		$model->unsetAttributes();

		if (isset($_GET['Measure'])) {
			$model->attributes = $_GET['Measure'];
		}

		$this->render('admin', array('model' => $model,));
	}

	public function loadModel($id) {
		$model = Measure::model()->findByPk($id);
		if ($model === null)
			throw new CHttpException(404, Yii::t('crud', 'The requested page does not exist.'));
		return $model;
	}

	protected function performAjaxValidation($model) {
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'measure-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
