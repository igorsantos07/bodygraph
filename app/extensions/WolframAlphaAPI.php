<?php
class WolframAlphaAPI {

	const API_URL = 'http://api.wolframalpha.com/v2/query';
	const API_KEY = 'A97KEV-V9EVJE73RL';

	/** @var SimpleXMLElement */
	protected $response;

	public function queryPlainText($input) {
		$querystring = http_build_query([
			'appid'  => self::API_KEY,
			'input'  => $input,
			'format' => 'plaintext'
		]);
		$this->response = simplexml_load_string(file_get_contents(self::API_URL.'?'.$querystring));
	}

	public function getDescription() {
		$pod = $this->getPod('Input interpretation');
		if ($pod)   return strip_tags($pod->xpath('subpod/plaintext')[0]->asXML());
		else        return false;
	}

	public function getMainResult() {
		$pod = $this->getPod('Results');
		if ($pod)   return strip_tags($pod->xpath('subpod/plaintext')[0]->asXML());
		else		return false;
	}

	private function getPod($title) {
		$pod = $this->response->xpath("/queryresult/pod[@title='$title']");
		if (is_array($pod) && isset($pod[0]))
			return $pod[0];
		else
			return false;
	}

}